from flask import Flask, render_template, abort

app = Flask(__name__)

@app.route("/<path:inputAddress>")  #For any request of a path after localhost:5000/<whatever> it'll store it into a path
def gotopage(inputAddress):         #This is the function to attempt to go to the page
    if (inputAddress.find('//') != -1 or inputAddress.find('..') != -1 or inputAddress.find('~') != -1): #This if block right here attempts to filter out any illegal addresses. flask seems to remove multiple '/'s at the start of an address, so it won't catch that, but it catches anything else.
        abort(403)  #If there was an illegal address, jump out of this route and go to the 403 errorhandler route
    else:           #If the address was legal, we'll either serve the page or a 404 error
        try:        #We're using a try except block to automatically catch when there is no page by capturing the flask error
            return render_template(inputAddress)  #This will attempt to return a page in templates/ at the input and throw an error if there is nothing
        except:         #If the error was raised,
            abort(404)  #jump out of this route and go to the 404 errorhandler route
        abort(404)      #If nothing so far has worked, then everything has gone wrong and we'll just throw a 404 in case

@app.errorhandler(404)   #This is flasks automatic errorhandler, this will automatically be called in case of 404 errors
def pagenotfound(error): 
    return render_template('404.html'), 404 #This will display the 404.html page in templates/ in the browser

@app.errorhandler(403)   #This is flasks automatic errorhandler, this will automatically be called in case of 403 errors
def pagenotfound(error):
    return render_template('403.html'), 403 #This will display the 403.html page in templates/ in the browser

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
