# PROJ2-PAGESERVER README 

Author: Andrew Evelhoch
Contact: aevelhoc@uoregon.edu

## DESCRIPTION
### The goals of this project are:

* Gain experience in using docker, both downloading images and building your own and running them
* Gain experience in learning to use intermediate frameworks like flask
* Recreate our pageserver from the previous project in flask in a docker

### Functionality:

Once the bitbucket directory is cloned to a machine, one simply needs to use docker to build the image, using the command 'docker build -t pageserver .' and then use the command 'docker run -d -p 5000:5000 pageserver' to run it in the background. While it is running, one can then open a web browser and access the files in the templates/ folder in the repo by going to localhost:5000/*the path to that file*. If the path contains any illegal characters or substrings of characters (of which there are ~, '..' and '//') it will bring up a 403 forbidden page. If the path does not go to a page in the templates/ folder, then it will display a 404 not found page. Once done with the server, it can be stopped by entering 'docker stop ' and then the name of that specific instance of the server, found using 'docker ps -a'. If you want to remove it entirely, you can remove the instance with 'docker rm' and then the name of that instance, and then 'docker rmi pageserver'.

### What I did:

After looking at the docker file and the test app, and making sure I could run them and make them work, I changed the app to add the functionality from the previous project. Because of both a combination of flask making server functions easier and more knowledge about python for myself, I was able to complete this in many fewer lines of code in the app.py file, and in a way that should be system agnostic, unlike my previous project which failed to serve trivia.html on the grading machine. I also created a templates/ folder, as that is what flask uses to locate files it is going to serve up. I also edited the Dockerfile; although I did not to change any functionality, I simply put my name in it. I also created a credentials.ini file to turn in.
